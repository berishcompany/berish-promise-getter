export type ObjectOrFunctionOrPromiseOrFunctionOfPromise<T> = T | (() => T) | Promise<T> | (() => Promise<T>);

export async function promiseGet<T>(value: ObjectOrFunctionOrPromiseOrFunctionOfPromise<T>) {
  let result: T | Promise<T> = null;
  if (typeof value == 'function') {
    result = value();
  } else result = value;
  return result;
}
